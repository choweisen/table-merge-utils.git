# table-merge-utils

动态html表格格子操作工具集合，基于低代码平台的工具实践。适用于二维数组动态创建html表格，通过操作二维数组以及单元格的rospan和colspan来对表格单元格进行合并、拆分、新增、删除等操作，目前支持：

- insertRow 向上下添加行
- insertCol 向左右添加列
- delRow 删除行
- delCol 删除列
- splitH 水平拆分单元格
- splitV 垂直拆分单元格
- leftMerge 向左合并单元格
- rightMerge 向右合并单元格
- topMerge 向上合并单元格
- bottomMerge 向下合并单元格


## es使用

```sh
# 安装
npm install table-merge-utils

# 引入
import TableMergeUtils from "table-merge-utils"


# 1.1 用工具初始化表格数据 4*4
let rows1 = TableMergeUtils.init({ rows: 4, cols: 4 })

let option = {
    # 表格至少存在的行数: 小于该值将不能删除行
    minRetainRow: 0,
    # 表格至少存在的列数: 小于该值将不能删除列
    minRetainCol: 0,
    # 单元格水平拆分最小colspan: 小于该值将不能水平拆分单元格
    minSplitHcolspan: 2,
    # 单元格垂直拆分最小rowspan: 小于该值将不能垂直拆分单元格
    minSplitVrowspan: 2,
    # rowspan错位修复方式  0:不处理; 1:修复rowspan最小为1; 2: 添加空行;
    fixRowType:  1
}

let tableUtil1 = new TableMergeUtils(rows, option)


# 1.2 已有表格二维数据
let rows2 = [
    [{rowspan: 1, colspan: 1, width:10, height:10}],
    [{rowspan: 1, colspan: 1, width:10, height:10}]
]
let tableUti2 = new TableMergeUtils(rows2) 


# 2.vue视图渲染表格
<table>
    <tr v-for="(row, rowIndex) in tableUtil.rows" :key="rowIndex">
        <tdv-for="(td, colIndex) in row" :rowspan="td.rowspan" :colspan="td.colspan"></td>
    </tr>
</table>


# 3.操作工具

# 选中格子(rowIndex, colIndex) 更新按钮状态
tableUtil.getDisabledHandles(rowIndex, colIndex);

# 选中格子(rowIndex, colIndex) 向下添加行
tableUtil.insertRow(rowIndex， colIndex, 1)
# 选中格子(rowIndex, colIndex）水平拆分单元格
tableUtil.splitH(rowIndex， colIndex)
...
```

## browser使用

```sh
# 引入链接
<script src="https://unpkg.com/table-merge-utils@1.0.3/dist/table-merge-utils.min.js"></script>

# 初始化
var tableUtil = new TableMergeUtils(tableMergeUtils.init({ rows: 4, cols: 4 }))
...
```

## demo
[vue demo](https://gitee.com/choweisen/table-merge-utils/tree/master/demo/vue-demo)