'use strict';

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);
  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    enumerableOnly && (symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    })), keys.push.apply(keys, symbols);
  }
  return keys;
}
function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = null != arguments[i] ? arguments[i] : {};
    i % 2 ? ownKeys(Object(source), !0).forEach(function (key) {
      _defineProperty(target, key, source[key]);
    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) {
      Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
    });
  }
  return target;
}
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}
function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor);
  }
}
function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}
function _defineProperty(obj, key, value) {
  key = _toPropertyKey(key);
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}
function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;
  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }
  return target;
}
function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = _objectWithoutPropertiesLoose(source, excluded);
  var key, i;
  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);
    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }
  return target;
}
function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}
function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}
function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}
function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}
function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;
  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
  return arr2;
}
function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function _toPrimitive(input, hint) {
  if (typeof input !== "object" || input === null) return input;
  var prim = input[Symbol.toPrimitive];
  if (prim !== undefined) {
    var res = prim.call(input, hint || "default");
    if (typeof res !== "object") return res;
    throw new TypeError("@@toPrimitive must return a primitive value.");
  }
  return (hint === "string" ? String : Number)(input);
}
function _toPropertyKey(arg) {
  var key = _toPrimitive(arg, "string");
  return typeof key === "symbol" ? key : String(key);
}

function getTableInfo(tableRows) {
  var _rows = JSON.parse(JSON.stringify(tableRows));
  var colsLength = _rows[0].reduce(function (total, cur) {
    return total + cur.colspan;
  }, 0);
  var colsHeight = Array.from({
    length: colsLength
  }, function () {
    return 0;
  });
  var _cols = Array.from({
    length: colsLength
  }, function () {
    return [];
  });
  _rows.forEach(function (rows, rowI) {
    rows.forEach(function (item, colI) {
      item._row = rowI;
      item._col = colI;
      item._startRow = rowI;
      item._endRow = rowI + item.rowspan - 1;
      var _colsHeight$reduce = colsHeight.reduce(function (min, col, i) {
          return col < min.height ? {
            index: i,
            height: col
          } : min;
        }, {
          index: 0,
          height: colsHeight[0]
        }),
        index = _colsHeight$reduce.index;
      var offset = item.colspan;
      while (offset > 0) {
        var _index = index + offset - 1;
        if (_index < colsLength) {
          _cols[_index].push(_objectSpread2({}, item));
          colsHeight[_index] += item.rowspan;
        }
        offset--;
      }
      item._startCol = index;
      item._endCol = item._startCol + item.colspan - 1;
    });
  });
  return {
    _rows: _rows,
    _cols: _cols
  };
}
function getCellOffset(start, end, targetStart, targetEnd) {
  if (end < targetStart || start > targetEnd) {
    return false;
  }
  var result = {
    start: start === targetStart,
    end: end === targetEnd,
    pass: start >= targetStart && end <= targetEnd,
    common: Math.min(end, targetEnd) - Math.max(start, targetStart) + 1,
    comCol: [Math.max(start, targetStart), Math.min(end, targetEnd)]
  };
  return result;
}
function findLeftRightCell(rows, row, col, offset) {
  var _getTableInfo2 = getTableInfo(rows),
    _rows = _getTableInfo2._rows;
  var curCell = _rows[row][col];
  var targetCell = _rows[row][col + offset];
  var isLeft = targetCell && targetCell.rowspan === curCell.rowspan && offset === -1 && targetCell._endCol + 1 === curCell._startCol;
  var isRight = targetCell && targetCell.rowspan === curCell.rowspan && offset === 1 && targetCell._startCol - 1 === curCell._endCol;
  if (isLeft || isRight) {
    return rows[row][col + offset];
  }
  return null;
}
function findTopBottomCell(rows, row, col, offset) {
  var _getTableInfo3 = getTableInfo(rows),
    _rows = _getTableInfo3._rows;
  var curCell = _rows[row][col];
  var targetCell = null;
  var _row = null;
  var _col = null;
  _rows.forEach(function (rowData, ri) {
    rowData.forEach(function (item, ci) {
      var colResult = getCellOffset(item._startCol, item._endCol, curCell._startCol, curCell._endCol);
      if (colResult.start && colResult.end) {
        if (offset === -1 && item._endRow + 1 === curCell._startRow) {
          targetCell = rows[ri][ci];
          _row = ri;
          _col = ci;
        } else if (offset === 1 && item._startRow - 1 === curCell._endRow) {
          targetCell = rows[ri][ci];
          _row = ri;
          _col = ci;
        }
      }
    });
  });
  return {
    targetCell: targetCell,
    row: _row,
    col: _col
  };
}
function updateRowspan(rows) {
  rows.forEach(function (row) {
    if (row.length > 1) {
      var minRowspan = row.reduce(function (min, cur) {
        return Math.min(min, cur.rowspan);
      }, row[0].rowspan);
      var disRowspan = minRowspan - 1;
      if (disRowspan > 0) {
        row.forEach(function (item) {
          return item.rowspan -= disRowspan;
        });
      }
    }
  });
}
function patchNullRow(rows) {
  var i = 0;
  rows.forEach(function (item, index) {
    var needFix = item.length && item.every(function (i) {
      return i.rowspan > 1;
    });
    if (needFix) {
      rows.splice(index + i + 1, 0, []);
      i++;
    }
  });
}

var _excluded = ["rows", "cols"];
var CELL_DEFAULT_CONFIG = {
  rowspan: 1,
  colspan: 1,
  width: 100,
  height: 40
};
var TableMergeUtils = /*#__PURE__*/function () {
  function TableMergeUtils(rows) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    _classCallCheck(this, TableMergeUtils);
    _defineProperty(this, "rows", []);
    _defineProperty(this, "options", {
      minRetainRow: 0,
      minRetainCol: 0,
      minSplitHcolspan: 2,
      minSplitVrowspan: 2,
      fixRowType: 1
    });
    this.rows = rows;
    this.options = _objectSpread2(_objectSpread2({}, this.options), options);
  }
  _createClass(TableMergeUtils, [{
    key: "getDisabledHandles",
    value: function getDisabledHandles() {
      var row = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : -1;
      var col = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : -1;
      var disabledHandles = [];
      var curCell = this.rows[row] && this.rows[row][col];
      if (curCell) {
        var totalRows = this.rows.reduce(function (total, cur) {
          return total + Math.min.apply(Math, _toConsumableArray(cur.map(function (item) {
            return item.rowspan;
          })));
        }, 0);
        if (totalRows - curCell.rowspan < this.options.minRetainRow) {
          disabledHandles.push('delRow');
        }
        var totalCols = this.rows[0].reduce(function (total, cur) {
          return total + cur.colspan;
        }, 0);
        if (totalCols - curCell.colspan < this.options.minRetainCol) {
          disabledHandles.push('delCol');
        }
        if (curCell.colspan < this.options.minSplitHcolspan) {
          disabledHandles.push('splitH');
        }
        if (curCell.rowspan < this.options.minSplitVrowspan) {
          disabledHandles.push('splitV');
        }
        if (!findLeftRightCell(this.rows, row, col, -1)) {
          disabledHandles.push('leftMerge');
        }
        if (!findLeftRightCell(this.rows, row, col, 1)) {
          disabledHandles.push('rightMerge');
        }
        if (!findTopBottomCell(this.rows, row, col, -1).targetCell) {
          disabledHandles.push('topMerge');
        }
        if (!findTopBottomCell(this.rows, row, col, 1).targetCell) {
          disabledHandles.push('bottomMerge');
        }
      } else {
        disabledHandles = ['insertRow', 'insertCol', 'delRow', 'delCol', 'splitH', 'splitV', 'leftMerge', 'rightMerge', 'topMerge', 'bottomMerge'];
      }
      return disabledHandles;
    }
  }, {
    key: "handFixRow",
    value: function handFixRow() {
      if (this.options.fixRowType === 1) {
        updateRowspan(this.rows);
      } else if (this.options.fixRowType === 2) {
        patchNullRow(this.rows);
      }
    }
  }, {
    key: "insertRow",
    value: function insertRow(row, col, offset) {
      var _this = this;
      if (this.getDisabledHandles(row, col).includes('insertRow')) {
        return;
      }
      var _getTableInfo = getTableInfo(this.rows),
        _rows = _getTableInfo._rows;
      var curCell = this.rows[row][col];
      var targetRowIndex = offset ? row + curCell.rowspan : row;
      if (targetRowIndex < _rows.length) {
        _rows.forEach(function (rowData, rowIndex) {
          if (rowIndex < targetRowIndex) {
            rowData.forEach(function (item) {
              var _offset = getCellOffset(item._startRow, item._endRow, targetRowIndex, targetRowIndex);
              if (_offset) {
                _this.rows[item._row][item._col].rowspan++;
                _this.rows[item._row][item._col].height += CELL_DEFAULT_CONFIG.height;
              }
            });
          }
        });
        var newRow = this.rows[targetRowIndex].map(function (item) {
          return _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), {}, {
            width: item.width,
            colspan: item.colspan
          });
        });
        this.rows.splice(targetRowIndex, 0, newRow);
      } else {
        var _newRow = this.rows[this.rows.length - 1].map(function (item) {
          return _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), {}, {
            width: item.width,
            colspan: item.colspan
          });
        });
        this.rows.splice(targetRowIndex, 0, _newRow);
      }
      var resultRow = offset ? row : row + 1;
      return {
        rows: this.rows,
        row: resultRow,
        col: col,
        disabledHandles: this.getDisabledHandles(resultRow, col)
      };
    }
  }, {
    key: "insertCol",
    value: function insertCol(row, col, offset) {
      var _this2 = this;
      if (this.getDisabledHandles(row, col).includes('insertCol')) {
        return;
      }
      var _getTableInfo2 = getTableInfo(this.rows),
        _rows = _getTableInfo2._rows;
      var curCell = _rows[row][col];
      _rows.forEach(function (rowData, rowIndex) {
        if (row === rowIndex) {
          _this2.rows[rowIndex].splice(col + offset, 0, _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), {}, {
            rowspan: curCell.rowspan
          }));
        } else {
          if (offset) {
            var _index = -1;
            rowData.forEach(function (item, index) {
              if (getCellOffset(item._startCol, item._endCol, curCell._startCol, curCell._endCol)) {
                _index = index;
              }
            });
            if (_index > -1) {
              var cell = rowData[_index];
              var _getCellOffset = getCellOffset(cell._startCol, cell._endCol, curCell._startCol, curCell._endCol),
                end = _getCellOffset.end;
              if (end) {
                _this2.rows[rowIndex].splice(_index + offset, 0, _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), {}, {
                  rowspan: cell.rowspan,
                  colspan: 1
                }));
              } else {
                _this2.rows[cell._row][cell._col].colspan++;
                _this2.rows[cell._row][cell._col].width += CELL_DEFAULT_CONFIG.width;
              }
            }
          } else {
            var _index2 = rowData.findIndex(function (item) {
              return getCellOffset(item._startCol, item._endCol, curCell._startCol, curCell._endCol);
            });
            if (_index2 > -1) {
              var _cell = rowData[_index2];
              var _getCellOffset2 = getCellOffset(_cell._startCol, _cell._endCol, curCell._startCol, curCell._endCol),
                start = _getCellOffset2.start;
              if (start) {
                _this2.rows[rowIndex].splice(_index2 + offset, 0, _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), {}, {
                  rowspan: _cell.rowspan,
                  colspan: 1
                }));
              } else {
                _this2.rows[_cell._row][_cell._col].colspan++;
                _this2.rows[_cell._row][_cell._col].width += CELL_DEFAULT_CONFIG.width;
              }
            }
          }
        }
      });
      var resultCol = offset ? col : col + 1;
      return {
        rows: this.rows,
        row: row,
        col: resultCol,
        disabledHandles: this.getDisabledHandles(row, resultCol)
      };
    }
  }, {
    key: "delRow",
    value: function delRow(row, col) {
      var _this3 = this,
        _this$rows;
      if (this.getDisabledHandles(row, col).includes('delRow')) {
        return;
      }
      var _getTableInfo3 = getTableInfo(this.rows),
        _rows = _getTableInfo3._rows;
      var curCell = _rows[row][col];
      _rows.forEach(function (rowData, rowIndex) {
        if (rowIndex < row) {
          rowData.forEach(function (item) {
            var _offset = getCellOffset(item._startRow, item._endRow, curCell._startRow, curCell._endRow);
            if (_offset) {
              _this3.rows[item._row][item._col].rowspan -= _offset.common;
              _this3.rows[item._row][item._col].height -= curCell.height;
            }
          });
        } else if (rowIndex > row) {
          var delIndex = [];
          rowData.forEach(function (item, index) {
            var _offset = getCellOffset(item._startRow, item._endRow, curCell._startRow, curCell._endRow);
            if (_offset) {
              delIndex.push(index);
              if (!_offset.pass) {
                var nextRowAddCell = _objectSpread2(_objectSpread2({}, _this3.rows[item._row][item._col]), {}, {
                  rowspan: item.rowspan - _offset.common
                });
                var _findTopBottomCell = findTopBottomCell(_rows, row, col, 1),
                  nextCellRow = _findTopBottomCell.row,
                  nextCellCol = _findTopBottomCell.col;
                _this3.rows[nextCellRow].splice(nextCellCol + 1, 0, nextRowAddCell);
              }
            }
          });
          var rowNewCells = _this3.rows[rowIndex].filter(function (item, index) {
            return !delIndex.includes(index);
          });
          _this3.rows.splice(rowIndex, 1, rowNewCells);
        }
      });
      var curCellnextCellRow = row + curCell.rowspan;
      _rows[row].forEach(function (item) {
        var _getTableInfo4 = getTableInfo(_this3.rows),
          _rows = _getTableInfo4._rows;
        if (item.rowspan > curCell.rowspan) {
          var targetCellCol = _rows[curCellnextCellRow].findIndex(function (nextRowItem) {
            return nextRowItem._startCol > item._endCol;
          });
          if (targetCellCol === -1) {
            _rows[curCellnextCellRow].forEach(function (nextRowItem, index) {
              if (nextRowItem._endCol < item._startCol) {
                targetCellCol = index + 1;
              }
            });
          }
          _this3.rows[curCellnextCellRow].splice(targetCellCol, 0, _objectSpread2(_objectSpread2({}, item), {}, {
            rowspan: item.rowspan - curCell.rowspan,
            height: item.height - curCell.height
          }));
        }
      });
      this.rows.splice(row, 1);
      var resultRows = this.rows.filter(function (item) {
        return item.length !== 0;
      });
      (_this$rows = this.rows).splice.apply(_this$rows, [0, this.rows.length].concat(_toConsumableArray(resultRows)));
      return {
        rows: this.rows,
        row: -1,
        col: -1,
        disabledHandles: this.getDisabledHandles(-1, -1)
      };
    }
  }, {
    key: "delCol",
    value: function delCol(row, col) {
      var _this4 = this,
        _this$rows2;
      if (this.getDisabledHandles(row, col).includes('delCol')) {
        return;
      }
      var _getTableInfo5 = getTableInfo(this.rows),
        _rows = _getTableInfo5._rows;
      var curCell = _rows[row][col];
      _rows.forEach(function (rowData, rowIndex) {
        var delIndex = [];
        rowData.forEach(function (item, index) {
          var _offset = getCellOffset(item._startCol, item._endCol, curCell._startCol, curCell._endCol);
          if (_offset) {
            if (_offset.pass) {
              delIndex.push(index);
            } else {
              _this4.rows[item._row][item._col].colspan -= _offset.common;
              _this4.rows[item._row][item._col].width -= curCell.width;
            }
          }
        });
        var rowNewCells = _this4.rows[rowIndex].filter(function (item, index) {
          return !delIndex.includes(index);
        });
        if (rowIndex < _this4.rows.length) {
          _this4.rows.splice(rowIndex, 1, rowNewCells);
        } else {
          _this4.rows.splice(rowIndex - _this4.rows.length, 1, rowNewCells);
        }
      });
      if (this.rows.every(function (item) {
        return item.length === 1;
      })) {
        this.rows.forEach(function (row) {
          return row.forEach(function (item) {
            return item.rowspan = 1;
          });
        });
      }
      var resultRows = this.rows.filter(function (item) {
        return item.length !== 0;
      });
      (_this$rows2 = this.rows).splice.apply(_this$rows2, [0, this.rows.length].concat(_toConsumableArray(resultRows)));
      this.handFixRow();
      return {
        rows: this.rows,
        row: -1,
        col: -1,
        disabledHandles: this.getDisabledHandles(-1, -1)
      };
    }
  }, {
    key: "splitH",
    value: function splitH(row, col) {
      var _this5 = this;
      if (this.getDisabledHandles(row, col).includes('splitH')) {
        return;
      }
      var _getTableInfo6 = getTableInfo(this.rows),
        _rows = _getTableInfo6._rows,
        _cols = _getTableInfo6._cols;
      var curCell = this.rows[row][col];
      var _curCell = _rows[row][col];
      if (curCell.colspan > 1) {
        var initColspan = _curCell.colspan;
        curCell.colspan -= Math.floor(initColspan / 2);
        var curCellStartCol = _curCell._startCol;
        var curCellEndCol = _curCell._startCol + curCell.colspan - 1;
        var curCellWidth = 0;
        while (curCellStartCol <= curCellEndCol) {
          var baseCell = _cols[curCellStartCol].find(function (i) {
            return i.colspan === 1;
          });
          curCellWidth += baseCell.width;
          curCellStartCol++;
        }
        curCell.width = curCellWidth;
        var addCellColspan = initColspan - curCell.colspan;
        var addCellStartCol = curCellEndCol + 1;
        var addCellEndCol = addCellStartCol + addCellColspan - 1;
        var addCellWidth = 0;
        while (addCellStartCol <= addCellEndCol) {
          var _baseCell = _cols[addCellStartCol].find(function (i) {
            return i.colspan === 1;
          });
          addCellWidth += _baseCell.width;
          addCellStartCol++;
        }
        this.rows[row].splice(col + 1, 0, _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), {}, {
          width: addCellWidth,
          height: curCell.height,
          colspan: addCellColspan,
          rowspan: curCell.rowspan
        }));
      } else {
        _rows.forEach(function (rowData, rowIndex) {
          if (rowIndex === row) {
            curCell.width /= 2;
            _this5.rows[row].splice(col + 1, 0, _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), {}, {
              width: curCell.width,
              height: curCell.height,
              rowspan: curCell.rowspan
            }));
          } else {
            var _colIndex = rowData.findIndex(function (item) {
              return getCellOffset(item._startCol, item._endCol, _curCell._startCol, _curCell._endCol);
            });
            if (_colIndex > -1) {
              _this5.rows[rowIndex][_colIndex].colspan += 1;
            }
          }
        });
      }
      return {
        rows: this.rows,
        row: row,
        col: col,
        disabledHandles: this.getDisabledHandles(row, col)
      };
    }
  }, {
    key: "splitV",
    value: function splitV(row, col) {
      var _this6 = this;
      if (this.getDisabledHandles(row, col).includes('splitV')) {
        return;
      }
      var _getTableInfo7 = getTableInfo(this.rows),
        _rows = _getTableInfo7._rows;
      var _curCell = _rows[row][col];
      var curCell = this.rows[row][col];
      if (curCell.rowspan > 1) {
        var initRowspan = _curCell.rowspan;
        curCell.rowspan -= Math.floor(initRowspan / 2);
        var curCellStartRow = _curCell._startRow;
        var curCellEndRow = _curCell._startRow + curCell.rowspan - 1;
        var curCellHeight = 0;
        while (curCellStartRow <= curCellEndRow) {
          var baseCell = _rows[curCellStartRow].find(function (i) {
            return i.rowspan === 1;
          });
          curCellHeight += baseCell.height;
          curCellStartRow++;
        }
        curCell.height = curCellHeight;
        var addCellRowspan = initRowspan - curCell.rowspan;
        var addCellStartRow = curCellEndRow + 1;
        var addCellEndRow = addCellStartRow + addCellRowspan - 1;
        var addCellHeight = 0;
        while (addCellStartRow <= addCellEndRow) {
          var _baseCell2 = _rows[addCellStartRow].find(function (i) {
            return i.rowspan === 1;
          });
          addCellHeight += _baseCell2.height;
          addCellStartRow++;
        }
        var targetCellrow = row + curCell.rowspan;
        var targetCellCol = _rows[targetCellrow].findIndex(function (item) {
          return item._startCol > _curCell._endCol;
        });
        if (targetCellCol === -1) {
          _rows[targetCellrow].forEach(function (item, index) {
            if (item._endCol < _curCell._startCol) {
              targetCellCol = index;
            }
          });
          this.rows[targetCellrow].splice(targetCellCol + 1, 0, _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), {}, {
            rowspan: addCellRowspan,
            colspan: curCell.colspan,
            width: curCell.width,
            height: addCellHeight
          }));
        } else {
          this.rows[targetCellrow].splice(targetCellCol, 0, _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), {}, {
            rowspan: addCellRowspan,
            colspan: curCell.colspan,
            width: curCell.width,
            height: addCellHeight
          }));
        }
      } else {
        curCell.height /= 2;
        this.rows.splice(row + 1, 0, Array.from({
          length: 1
        }, function () {
          return _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), {}, {
            colspan: curCell.colspan,
            width: curCell.width,
            height: curCell.height
          });
        }));
        _rows.forEach(function (item, rowIndex) {
          item.forEach(function (i, colIndex) {
            if (rowIndex !== row || colIndex !== col) {
              var offset = getCellOffset(i._startRow, i._endRow, _curCell._startRow, _curCell._endRow);
              if (offset) {
                _this6.rows[i._row][i._col].rowspan++;
              }
            }
          });
        });
      }
      return {
        rows: this.rows,
        row: row,
        col: col,
        disabledHandles: this.getDisabledHandles(row, col)
      };
    }
  }, {
    key: "leftMerge",
    value: function leftMerge(row, col) {
      if (this.getDisabledHandles(row, col).includes('leftMerge')) {
        return;
      }
      var leftCell = findLeftRightCell(this.rows, row, col, -1);
      if (leftCell) {
        var curCell = this.rows[row][col];
        leftCell.colspan += curCell.colspan;
        leftCell.width += curCell.width;
        this.rows[row].splice(col, 1);
        return {
          rows: this.rows,
          row: row,
          col: col - 1,
          disabledHandles: this.getDisabledHandles(row, col - 1)
        };
      }
    }
  }, {
    key: "rightMerge",
    value: function rightMerge(row, col) {
      if (this.getDisabledHandles(row, col).includes('rightMerge')) {
        return;
      }
      var rightCell = findLeftRightCell(this.rows, row, col, 1);
      if (rightCell) {
        var curCell = this.rows[row][col];
        curCell.colspan += rightCell.colspan;
        curCell.width += rightCell.width;
        this.rows[row].splice(col + 1, 1);
        return {
          rows: this.rows,
          row: row,
          col: col,
          disabledHandles: this.getDisabledHandles(row, col)
        };
      }
    }
  }, {
    key: "topMerge",
    value: function topMerge(row, col) {
      if (this.getDisabledHandles(row, col).includes('topMerge')) {
        return;
      }
      var _findTopBottomCell2 = findTopBottomCell(this.rows, row, col, -1),
        topCell = _findTopBottomCell2.targetCell,
        _row = _findTopBottomCell2.row,
        _col = _findTopBottomCell2.col;
      if (topCell) {
        if (this.rows[row].length === 1) {
          this.delRow(this.rows, row, col);
        } else {
          var curCell = this.rows[row][col];
          topCell.rowspan += curCell.rowspan;
          topCell.height += curCell.height;
          this.rows[row].splice(col, 1);
        }
        return {
          rows: this.rows,
          row: _row,
          col: _col,
          disabledHandles: this.getDisabledHandles(_row, _col)
        };
      }
    }
  }, {
    key: "bottomMerge",
    value: function bottomMerge(row, col) {
      if (this.getDisabledHandles(row, col).includes('bottomMerge')) {
        return;
      }
      var _findTopBottomCell3 = findTopBottomCell(this.rows, row, col, 1),
        bottomCell = _findTopBottomCell3.targetCell,
        _row = _findTopBottomCell3.row,
        _col = _findTopBottomCell3.col;
      if (bottomCell) {
        if (this.rows[_row].length === 1) {
          this.delRow(_row, _col);
        } else {
          var curCell = this.rows[row][col];
          curCell.rowspan += bottomCell.rowspan;
          curCell.height += bottomCell.height;
          this.rows[_row].splice(_col, 1);
        }
        return {
          rows: this.rows,
          row: row,
          col: col,
          disabledHandles: this.getDisabledHandles(row, col)
        };
      }
    }
  }], [{
    key: "init",
    value: function init(_ref) {
      var rows = _ref.rows,
        cols = _ref.cols,
        rest = _objectWithoutProperties(_ref, _excluded);
      return Array.from({
        length: rows
      }, function () {
        return Array.from({
          length: cols
        }, function () {
          return _objectSpread2(_objectSpread2({}, CELL_DEFAULT_CONFIG), rest);
        });
      });
    }
  }]);
  return TableMergeUtils;
}();

module.exports = TableMergeUtils;
