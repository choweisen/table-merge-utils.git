interface cell {
  rowspan: number;
  colspan: number;
  width: number;
  height: number;
}

interface initTableOption {
  rows: number;
  cols: number;
  width?: number;
  height?: number;
}

interface tableOptions {
  minRetainRow?: number,
  minRetainCol?: number,
  minSplitHcolspan?: number,
  minSplitVrowspan?: number,
  fixRowType?: 0 | 1 | 2,
}
interface handleRes {
  rows: cell[][];
  row: number;
  col: number;
  disabledHandles: string[];
}

declare class TableMergeUtils {
  rows: cell[][];
  constructor(rows: cell[][], options?: tableOptions);
  static init(option: initTableOption): cell[][];
  getDisabledHandles(row?: number, col?: number): string[];
  insertCol(row: number, col: number, offset: number): handleRes;
  insertRow(row: number, col: number, offset: number): handleRes;
  delRow(row: number, col: number): handleRes;
  delCol(row: number, col: number): handleRes;
  splitH(row: number, col: number): handleRes;
  splitV(row: number, col: number): handleRes;
  leftMerge(row: number, col: number): handleRes | void;
  rightMerge(row: number, col: number): handleRes | void;
  topMerge(row: number, col: number): handleRes | void;
  bottomMerge(row: number, col: number): handleRes | void;
}

export default TableMergeUtils;
