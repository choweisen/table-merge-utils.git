import resolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';
import terser from '@rollup/plugin-terser';
import json from '@rollup/plugin-json';
import cleanup from 'rollup-plugin-cleanup';
import pkg from './package.json' assert {type: 'json'};

export default {
    input: 'src/index.js',
    output: [
        {
            file: 'dist/table-merge-utils.es.js',
            format: 'es',
            name: pkg.name
        },
        {
            file: 'dist/table-merge-utils.min.js',
            format: 'iife',
            name: 'TableMergeUtils',
            plugins: [terser()]
        },
        {
            file: 'dist/table-merge-utils.cjs.js',
            format: 'cjs',
            name: pkg.name
        },
    ],
    plugins: [
        resolve(),
        json(),
        cleanup(),
        babel({ babelHelpers: 'bundled' })
    ]
};